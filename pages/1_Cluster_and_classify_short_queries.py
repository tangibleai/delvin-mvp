import streamlit as st
import pandas as pd
import numpy as np
from pathlib import Path
from hyperopt import hp
from sklearn.metrics.cluster import adjusted_rand_score

import time
import random
from sentence_transformers import SentenceTransformer

from chatintents import ChatIntents

########################################################################################################################
#-----------------------------------------------CONSTANTS--------------------------------------------------------------#
########################################################################################################################

PAGE_TITLE = "Short Queries Clustering"
INTRO_TEXT = "This application helps you find common trends in chatbot or search queries" \
            "entered by your users. By the end of the process, you'll be able to understand " \
            "what are the most common queries, and even create a model that will help you classify" \
            "new queries!"
STEP_1_HEADER = "Step 1 - Upload your data"
STEP_2_HEADER = "Step 2 - Examine your data"
STEP_3_HEADER = "Step 3 - Explore clusters in your data"

########################################################################################################################
#-----------------------------------------------------APP--------------------------------------------------------------#
########################################################################################################################

st.set_page_config(page_title=PAGE_TITLE)

st.title(PAGE_TITLE)
st.markdown(INTRO_TEXT)

st.header(STEP_1_HEADER)
# Create a file uploader
uploaded_file = st.file_uploader("Upload a CSV file, with text answers is a column named 'text'", type="csv")