import pandas as pd
import streamlit as st
from keybert import KeyBERT
from keyphrase_vectorizers import KeyphraseCountVectorizer, KeyphraseTfidfVectorizer
import hdbscan
import uuid

import plotly.graph_objects as go


INTRO_TEXT = "This application helps you explore longer text pieces, " \
            "such as open question answers, evaluation survey answers, and more."
STEP_1_HEADER = "Step 1 - Upload your data"
STEP_2_HEADER = "Step 2 - Clarify your analysis goal"
STEP_2_TEXT = "Before starting your thematic analysis, it's helpful to put on paper what you know about your data  " \
              "and what is the goal of your analysis. This will help you choose the right method and guide the " \
              "language processing algorithms."
STEP_3_HEADER = "Step 3 - Choose your analysis method"


TAB_1_STEP_4_HEADER = "Step 4 - Extract keywords."
TAB_1_STEP_4_TEXT = "We\'ll start by extracting **keywords** - words that describe the essense of the answer. " \
                    "Seeing which keywords are mentioned will help us get a sense of popular themes in our dataset. "

TAB_1_STEP_5_HEADER = "Step 5 - Group keywords into themes."
TAB_1_STEP_5_TEXT = "We can now cluster the keywords that are semantically close to each other into themes. "
TAB_1_STEP_6_HEADER = "Step 6 - Find theme distribution in the dataset."

TAB_2_STEP_4_HEADER = "Step 4 - Take an initial look at your data"
TAB_2_STEP_4_TEXT = "First, you'll pick some samples from your data and create your thematic categories"

TAB_2_STEP_5_HEADER = "Step 5 - Propagate your labels "
TAB_2_STEP_4_TEXT = "So, you have a small sample of content that's labels "

#-----------------------------------------FUNCTIONS--------------------------------------------------------------------
@st.cache(allow_output_mutation=True)
def load_keybert():
    model = KeyBERT()
    return model
def extract_keywords(text_column, kw_model= None, vectorizer=None):
    docs = list(text_column)
    count_vectorizer = KeyphraseCountVectorizer()
    doc_embeddings, word_embeddings = kw_model.extract_embeddings(docs, vectorizer=count_vectorizer)

    return kw_model.extract_keywords(docs, vectorizer=count_vectorizer,
                                         doc_embeddings=doc_embeddings, word_embeddings=word_embeddings)


#---------------------------------------------APP-----------------------------------------------------------------------
st.set_page_config(page_title="Thematic Analysis")
if 'session_id' not in locals():
    session_id = uuid

# Add a title to the app
st.title("Thematic analysis ")
st.markdown(INTRO_TEXT)

st.header(STEP_1_HEADER)
# Create a file uploader
uploaded_file = st.file_uploader("Upload a CSV file, with text answers is a column named 'text'", type="csv")

df = pd.DataFrame()

# If a file was uploaded
if uploaded_file is not None:
    # Read the file and create a Pandas DataFrame
    df = pd.read_csv(uploaded_file)

    # Show the DataFrame
    st.write(df.head(4))

st.header(STEP_2_HEADER)
st.markdown(STEP_2_TEXT)
st.text_input("What do you aim to achieve through your analysis?")
st.text_input("What words do you expect to see in the text?")

st.header(STEP_3_HEADER)
tab1, tab2 = st.tabs(["Automated Analysis", "Human-Assisted Analysis"])

with tab1:
    st.header(TAB_1_STEP_4_HEADER)
    st.write(TAB_1_STEP_4_TEXT)

    if uploaded_file is not None:
        kw_model = load_keybert()
        keywords = extract_keywords(df['text'], kw_model=kw_model)
        df_keyword_list = []

        for i, doc in enumerate(keywords, start=1):
            # Extract the first element of each tuple and append it to the list with the document number
            for tup in doc:
                for tup in doc:
                    df_keyword_list.append([i, tup[0]])

        keyword_df = pd.DataFrame(df_keyword_list, columns=['document', 'keyword'])
        keyword_counts = keyword_df['keyword'].value_counts().sort_values(ascending=False)

        top_keywords = keyword_counts.head(15).iloc[::-1]

        fig = go.Figure(go.Bar(
            x=top_keywords.values,
            y=top_keywords.index,
            orientation='h'))

        # set the title and axis labels
        fig.update_layout(
            title="Most Frequent Keywords",
            xaxis_title="Number of documents",
        )

        st.plotly_chart(fig)
    else:
        st.markdown("*Load your text data to see top keywords distribution*")

    st.header(TAB_1_STEP_5_HEADER)

    if uploaded_file is not None:
        _, keyword_embeddings = kw_model.extract_embeddings(df['text'], candidates=list(keyword_counts.index))

        clusterer = hdbscan.HDBSCAN(min_cluster_size=3,
                                    min_samples=3,
                                    metric='euclidean',
                                    gen_min_span_tree=True,
                                    cluster_selection_method='eom')

        cluster_labels = clusterer.fit_predict(keyword_embeddings)

        unique_labels = set(cluster_labels)

        keyword_counts = pd.DataFrame({'keyword': keyword_counts.index, 'count': keyword_counts.values},
                                       index=keyword_counts.index)

        keyword_counts['cluster_num'] = cluster_labels

        for i in unique_labels:
            words = list(keyword_counts['keyword'][keyword_counts['cluster_num'] == i])
            cluster_name = ", ".join([words[0],words[1],words[2]])
            with st.expander(cluster_name):
                words = list(keyword_counts['keyword'][keyword_counts['cluster_num'] == i])
                for word in words:
                    st.write(f"{word} ({keyword_counts['count'].loc[word]})")
    else:
        st.markdown("*Load your text data to see keyword clusters*")

    st.header(TAB_1_STEP_6_HEADER)


with tab2:
    st.header(TAB_2_STEP_4_HEADER)
    st.markdown(TAB_2_STEP_4_TEXT)

    topics = ['topic 1', 'topic 2']
    with st.form("my_form"):
        st.text_area("Text sample",disabled=True)
        chosen_topics = st.multiselect("Choose relevant topics", topics)
        st.write('or')
        new_topic_input = st.text_input('Create a new topic ')
        st.form_submit_button("Next")






