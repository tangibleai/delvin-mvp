---
title: Delvin Mvp
emoji: 📈
colorFrom: pink
colorTo: yellow
sdk: streamlit
sdk_version: 1.19.0
app_file: app.py
pinned: false
license: agpl-3.0
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
